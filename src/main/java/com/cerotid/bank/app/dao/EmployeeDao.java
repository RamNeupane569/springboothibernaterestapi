package com.cerotid.bank.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cerotid.bank.app.entity.Employee;

@Repository
public interface EmployeeDao extends JpaRepository<Employee, Long> {

}

package com.cerotid.bank.app.service;

import java.util.List;

import com.cerotid.bank.app.entity.Employee;

public interface EmployeeService {

	Employee saveEmployee(Employee employee);
	
	Employee updateEmployee(Employee employee);
	
	List<Employee> getAllEmployeeList();
	
	Employee getEmployee(Long employeeId);
	
	void deleteEmployee(Long employeeId);
	
	
}

package com.cerotid.bank.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cerotid.bank.app.dao.EmployeeDao;
import com.cerotid.bank.app.entity.Employee;
import com.cerotid.bank.app.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;

	@Override
	public Employee saveEmployee(Employee employee) {
		return employeeDao.save(employee);
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		return employeeDao.saveAndFlush(employee);
	}

	@Override
	public List<Employee> getAllEmployeeList() {
		return employeeDao.findAll();
	}

	@Override
	public Employee getEmployee(Long employeeId) {
		return employeeDao.getOne(employeeId);
	}

	@Override
	public void deleteEmployee(Long employeeId) {
		employeeDao.deleteById(employeeId);
	}

}

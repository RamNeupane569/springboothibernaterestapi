package com.cerotid.bank.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class SpringBootHibernateRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHibernateRestApiApplication.class, args);
	}

}
